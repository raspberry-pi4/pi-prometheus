# Raspberry Pi Prometheus ecosystem

http://192.168.1.31:9090/ - Prometheus Console

http://192.168.1.31:9100/ - Node Exporter

http://192.168.1.31:9115/ - Blackbox Exporter

http://192.168.1.31:9090/targets - Blackbox Exporter Targets

http://192.168.1.31:9115/probe?module=http_2xx&target=http%3A%2F%2Fprometheus.io - Sample Probe

http://192.168.1.31:3000/ - Grafana console


# Setup

## Install Prometheus

```
sudo apt-get-install prometheus
```

## Install Grafana

For now (Jun-2020) `sudo apt-get install grafana`. 
It apparently installs a very old (like 3.x)
version of Grafana.

So we tried to download and install form the grafana site:
https://grafana.com/grafana/download?platform=arm

Version 7.x does not work because `glibc` is not up to date for the armf architecture,
as of Jun-2020.

We installed the latest 6.x (https://grafana.com/grafana/download/6.7.3) with:
```
sudo apt-get install -y adduser libfontconfig1
wget https://dl.grafana.com/oss/release/grafana_6.7.3_armhf.deb
sudo dpkg -i grafana_6.7.3_armhf.deb
```
**NOTE**: prometheus will up upgraded by apt, grafana will not.

## Support Tools

```
sudo apt-get install sqlite3

sudo apt-get install prometheus-blackbox-exporter 
```

## Blackbox exporter configuration

Around line 6, in the `http:` entry for the `http_2xx:` module configuration
we added the line:
```
      preferred_ip_protocol: ip4
```
because some part if the Pi networking configuration was
defaulting to using ipv6 addresses, which did not work
from our router.

This was discovered with the following:

```
systemctrl | grep blackbox
sudo journalctl -u prometheus-blackbox-exporter.service
```
The errors for our target hosts were failing, and the IP addresses printed
in the logs were ipv6 addresses. A quick internet serach showed the
solutoin was to prefer the ipv4 addresses with the configuartion line
above. Then restart with the new config:
```
sudo systemctl restart prometheus-blackbox-exporter
```

